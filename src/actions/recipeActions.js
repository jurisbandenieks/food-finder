import { GET_RECIPES, SEARCH_RECIPES } from "./types"
import axios from "axios"

export const getRecipes = () => {
  return {
    type: GET_RECIPES
  }
}

export const searchRecipes = search => async dispatch => {
  if (search !== "") {
    const appId = "a5de687e"
    const appKey = "333679036960672319f9e5f29837f1f3"

    const res = await axios.get(
      `https://api.edamam.com/search?q=${search}&app_id=${appId}&app_key=${appKey}`
    )

    const data = res.data.hits

    dispatch({
      type: SEARCH_RECIPES,
      payload: data
    })
  }
}
