import React, { useRef } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import { searchRecipes } from "../../actions/recipeActions"

const Header = ({ searchRecipes }) => {
  const text = useRef("")

  const onChange = e => {
    if (text.current.value !== "") {
      searchRecipes(text.current.value)
    }
  }

  return (
    <nav style={{ marginBottom: "30px" }} className="red">
      <div className="nav-wrapper">
        <form>
          <div className="input-field">
            <input
              id="search"
              type="search"
              placeholder="Search Recipes..."
              ref={text}
              onChange={onChange}
            />
            <label className="label-icon" htmlFor="search">
              <i className="material-icons">search</i>
            </label>
          </div>
        </form>
      </div>
    </nav>
  )
}

Header.propTypes = {
  searchRecipes: PropTypes.func.isRequired
}

export default connect(null, { searchRecipes })(Header)
