import React, { useEffect } from "react"

import RecipeItem from "./RecipeItem"

import { connect } from "react-redux"
import PropTypes from "prop-types"
import { getRecipes } from "../../actions/recipeActions"

const Recipes = ({ recipes, getRecipes }) => {
  useEffect(() => {
    getRecipes()
    // eslint-disable-next-line
  }, [])
  return (
    <div className="row">
      {!recipes.food ? (
        <p className="center">Please search recipe to display...</p>
      ) : (
        recipes.food.map((recipe, index) => (
          <RecipeItem recipe={recipe} key={index} />
        ))
      )}
    </div>
  )
}

Recipes.propTypes = {
  getRecipes: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  recipes: state.recipes
})

export default connect(mapStateToProps, { getRecipes })(Recipes)
