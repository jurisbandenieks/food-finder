import React from "react"

const RecipeItem = props => {
  const { recipe } = props.recipe
  return (
    <div className="col s12 m3">
      <div className="card">
        <div className="card-image ">
          <img src={recipe.image} />
          <span className="card-title ">{recipe.label}</span>
        </div>
        <div
          className="card-content"
          style={{ height: "10rem", overflowY: "scroll" }}
        >
          {recipe.ingredients.map((ingredient, index) => (
            <p key={index}>{ingredient.text}</p>
          ))}
        </div>
        <div className="card-action">
          <a href={recipe.url}>Recipe Link</a>
        </div>
      </div>
    </div>
  )
}

export default RecipeItem
