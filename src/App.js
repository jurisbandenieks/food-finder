import React, { useEffect } from "react"

import Header from "./components/layout/Header"
import Recipes from "./components/recipes/Recipes"

import { Provider } from "react-redux"
import store from "./store"

import "materialize-css/dist/css/materialize.min.css"
import M from "materialize-css/dist/js/materialize.min.js"
import "./App.css"

const App = () => {
  useEffect(() => {
    // Auto init Materialize
    M.AutoInit()
  })

  return (
    <Provider store={store}>
      <div className="App">
        <Header />
        <Recipes />
      </div>
    </Provider>
  )
}

export default App
