import { GET_RECIPES, SEARCH_RECIPES } from "../actions/types"

const initialState = {
  food: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_RECIPES:
      return {
        ...state,
        food: state.food
      }
    case SEARCH_RECIPES:
      return {
        ...state,
        food: action.payload
      }
    default:
      return state
  }
}
